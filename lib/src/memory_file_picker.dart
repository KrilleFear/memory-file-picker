/*
 *   Memory File Picker
 *   Copyright (C) 2019, 2020 Christian Pauly
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';

import 'memory_file.dart';

abstract class MemoryFilePicker {
  /// Returns a `MemoryFile` object from the selected file path.
  /// on Web [type], [allowedExtensions] and [onFileLoading] will
  /// be ignored.
  static Future<MemoryFile> getFile({
    FileType type = FileType.any,
    List<String> allowedExtensions,
    Function(dynamic) onFileLoading,
  }) async {
    final picked = await FilePicker.getFile(
      type: type,
      allowedExtensions: allowedExtensions,
      onFileLoading: onFileLoading,
    );
    if (picked == null) return null;
    return MemoryFile()
      ..bytes = await picked.readAsBytes()
      ..path = picked.path;
  }

  /// Returns a `MemoryFile` object from the selected image. On web
  /// [maxWidth], [maxHeight], [imageQuality], [source] and [preferredCameraDevice]
  /// will be ignored.
  static Future<MemoryFile> getImage({
    @required ImageSource source,
    double maxWidth,
    double maxHeight,
    int imageQuality,
    CameraDevice preferredCameraDevice = CameraDevice.rear,
  }) async {
    final picked = await ImagePicker().getImage(
      source: source,
      maxWidth: maxWidth,
      maxHeight: maxHeight,
      imageQuality: imageQuality,
      preferredCameraDevice: preferredCameraDevice,
    );
    if (picked == null) return null;
    return MemoryFile()
      ..bytes = await picked.readAsBytes()
      ..path = picked.path;
  }
}
