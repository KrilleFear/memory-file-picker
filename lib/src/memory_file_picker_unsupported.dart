import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'memory_file.dart';

abstract class MemoryFilePicker {
  /// Returns a `MemoryFile` object from the selected file path.
  /// on Web [type], [allowedExtensions] and [onFileLoading] will
  /// be ignored.
  static Future<MemoryFile> getFile({
    FileType type = FileType.any,
    List<String> allowedExtensions,
    Function(dynamic) onFileLoading,
  }) async {
    throw (Exception('Unsupported'));
  }

  /// Returns a `MemoryFile` object from the selected image. On web
  /// [maxWidth], [maxHeight], [imageQuality], [source] and [preferredCameraDevice]
  /// will be ignored.
  static Future<MemoryFile> getImage({
    @required ImageSource source,
    double maxWidth,
    double maxHeight,
    int imageQuality,
    CameraDevice preferredCameraDevice = CameraDevice.rear,
  }) async {
    throw (Exception('Unsupported'));
  }
}
