import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memoryfilepicker/memoryfilepicker.dart';

void main() {
  runApp(MemoryFilePickerTestApp());
}

class MemoryFilePickerTestApp extends StatefulWidget {
  @override
  _MemoryFilePickerTestAppState createState() =>
      _MemoryFilePickerTestAppState();
}

class _MemoryFilePickerTestAppState extends State<MemoryFilePickerTestApp> {
  MemoryFile file;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: file == null
              ? RaisedButton(
                  child: Text('Pick image'),
                  onPressed: () async {
                    final picked = await MemoryFilePicker.getImage(
                      source: ImageSource.gallery,
                    );
                    if (picked != null) {
                      setState(() => file = picked);
                    }
                  },
                )
              : MemoryImage(file.bytes),
        ),
      ),
    );
  }
}
