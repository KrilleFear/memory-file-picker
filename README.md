# Memory File Picker

A simple memory file picker for Flutter on Android, iOS and web.
This package makes it possible to use a file picker on Flutter for web while
it provides the same API for both web and mobile. On web it will read the file
using the dart HTML FileReader object while on mobile it just calls the 
FilePicker or ImagePicker packages.

## Getting Started

Just use this as a replacement for:

* (File Picker)[https://pub.dev/packages/file_picker]
* (Image Picker)[https://pub.dev/packages/image_picker]

The file will be loaded to the memory so make sure to not load too big files.