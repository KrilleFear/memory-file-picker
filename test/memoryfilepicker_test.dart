import 'package:flutter_test/flutter_test.dart';

import 'package:memoryfilepicker/memoryfilepicker.dart';

void main() {
  test('Test memory file', () {
    final file = MemoryFile()
      ..bytes = null
      ..path = '/nullfile';
    expect(file.bytes, null);
    expect(file.path, '/nullfile');
  });
}
