## [0.1.3] - 2020-09-08

* Implement FileType.image on web when using getFile

## [0.1.2] - 2020-06-20

* Switch to conditional imports
  
## [0.1.1] - 2020-06-20

* Minor fixes

## [0.1.0] - 2020-06-20

* First release